(ns gemclient.header
  (:require [clojure.string :as str]))

(defn- status-code [header]
  (-> header
      (str/split #" ")
      first
      Integer/parseInt))

(defn parse-header [header]
  (let [status-num (status-code header)]
    {:status status-num
     :meta (str/trim (str/replace-first header (str status-num) ""))}))
