(ns gemclient.core
  (:require [gemclient.request :as request]))

(defn request
  "Get the Gemini response from the provided path.
   This returns a map with the following keys:
   :status (int) :meta (string) :body (string, optional)"
  [url & {:keys [port] :or {port 1965}}]
  (request/load-content url :port port))

