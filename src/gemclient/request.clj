(ns gemclient.request
  (:import (java.io BufferedReader BufferedWriter
                    InputStreamReader OutputStreamWriter)
           (javax.net.ssl X509TrustManager SSLContext SSLSocket SSLSocketFactory))
  (:require [clojure.string :as str]
            [gemclient.url    :as url]
            [gemclient.header :as header]))

(defn new-socket-factory
  "Create a socket factory that ignores self-signed certs."
  ^SSLSocketFactory
  []
  (let [cert-manager (make-array X509TrustManager 1)
        sc           (SSLContext/getInstance "SSL")]
    (aset ^objects cert-manager 0
          (proxy [X509TrustManager][]
            (getAcceptedIssuers [])
            (checkClientTrusted [_ _])
            (checkServerTrusted [_ _])))
    (.init sc nil cert-manager (java.security.SecureRandom.))
    (.getSocketFactory sc)))

(defn- create-ssl-socket ^SSLSocket [host port]
  (let [f (new-socket-factory)
        ^SSLSocket s (.createSocket f ^String host ^Integer port)]
    (.startHandshake s)
    s))

(defn- write-body [^BufferedWriter w url]
  (let [body (str url "\r\n")]
    (.write w body 0 (count body))
    (.flush w)))

(defn- read-response-lines [^BufferedReader r]
  (loop [line   (.readLine r)
         result []]
    (if-not line
      result
      (recur (.readLine r)
             (conj result line)))))

(defn- read-response [^SSLSocket s]
  (let [r     (BufferedReader. (InputStreamReader. (.getInputStream  s)))
        lines (read-response-lines r)
        header (first lines)
        parsed-header (header/parse-header header)
        body (str/join \newline (drop 1 lines))]
    (.close r)
    (merge parsed-header
           (if body {:body body} {}))))

(defn- make-request [^SSLSocket s url]
  (let [w (BufferedWriter. (OutputStreamWriter. (.getOutputStream s)))]
    (write-body w url)              
    (let [resp (read-response s)] 
      (.close w)
      resp)))

(defn load-content
  "Make a request to a URL and port, returns response map."
  [url & {:keys [port] :or {port 1965}}]
  (let [s    (create-ssl-socket (url/hostname url) port)
        resp (make-request s (url/absolute url))]
    (.close s)
    resp))
